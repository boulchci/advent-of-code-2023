namespace AdventOfCode2023;

public abstract class APart
{
    protected string _fileText;
    protected List<string> _fileLines;
    public abstract void Run();

    protected void SetFile(string path)
    {
        _fileText = File.ReadAllText(path);
        _fileLines = File.ReadAllLines(path).ToList();
    }
}