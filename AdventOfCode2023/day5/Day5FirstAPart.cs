using System.Text.RegularExpressions;

namespace AdventOfCode2023.day5;

public class Day5FirstAPart : APart
{
    private List<long> _seeds = new();
    private List<List<string>> _rawMappings = new();

    public Day5FirstAPart()
    {
        SetFile("day5/input.txt");

        // remplissage des seeds
        var firstLine = _fileLines.First().Split(' ').ToList();
        firstLine.RemoveAt(0);
        firstLine.ForEach(x => _seeds.Add(long.Parse(x)));
        
        // lecture des mappings
        var chuncks = _fileText.Split("\n\n").ToList();
        chuncks.ForEach(x => _rawMappings.Add(x.Split('\n').ToList()));
        _rawMappings.RemoveAt(0);
        _rawMappings.ForEach(x => x.RemoveAt(0));
    }

    public override void Run()
    {
        List<long> mappingResults = _seeds;
        int mappingCategoriesDoneCount = 0;
        foreach (var rawMapping in _rawMappings)
        {
            List<long> currentMappingResults = new List<long>();
            int mappingResultDoneCount = 0;
            foreach (long mappingResult in mappingResults )
            {
                Console.Write($"\rMapping categories done {mappingCategoriesDoneCount}/{_rawMappings.Count} | Mapping results done {mappingResultDoneCount}/{mappingResults.Count}");
                currentMappingResults.Add(GetMappingResult(rawMapping, mappingResult));
                mappingResultDoneCount++;
            }

            mappingResults = currentMappingResults;
            mappingCategoriesDoneCount++;
        }

        mappingResults.Sort();
        
        Console.WriteLine($"\nLowest location is {mappingResults.First()}");
    }

    // public override void Run()
    // {
    //     List<Dictionary<long, long>> mappings = new List<Dictionary<long, long>>();
    //     foreach (var rawMapping in _rawMappings)
    //     {
    //         mappings.Add(GetMappings(rawMapping));   
    //     }
    //
    //     List<long> mappingResults = new List<long>();
    //     _seeds.ForEach(x => mappingResults.Add(Map(x, mappings, 0)));
    //     mappingResults.Sort();
    //     
    //     Console.WriteLine($"Lowest location is {mappingResults.First()}");
    // }

    private long GetMappingResult(List<string> rawMapping, long initialValue)
    {
        var detailedRawMappings = rawMapping.Select(x => x.Split(' ')).ToList();
        
        foreach (var detailedRawMapping in detailedRawMappings)
        {
            long source = long.Parse(detailedRawMapping[1]);
            long destination = long.Parse(detailedRawMapping[0]);

            for (long i = 0; i < long.Parse(detailedRawMapping[2]); i++)
            {
                if (source == initialValue) return destination;
                source++;
                destination++;
            }
        }

        return initialValue;
    }
    
    // private Dictionary<long, long> GetMapping(List<string> rawMapping, long value)
    // {
    //     var detailedRawMappings = rawMapping.Select(x => x.Split(' ')).ToList();
    //     Dictionary<long, long> mappings = new Dictionary<long, long>();
    //
    //     foreach (var detailedRawMapping in detailedRawMappings)
    //     {
    //         long source = long.Parse(detailedRawMapping[1]);
    //         long destination = long.Parse(detailedRawMapping[0]);
    //
    //         for (long i = 0; i < long.Parse(detailedRawMapping[2]); i++)
    //         {
    //             mappings.Add(source, destination);
    //             source++;
    //             destination++;
    //         }
    //     }
    //
    //     return mappings;
    // }

    private long Map(long initialValue, List<Dictionary<long, long>> mappings, int mappingIndex)
    {
        if (mappingIndex == mappings.Count) return initialValue; // fini la récursivité 🤢
        if (!mappings[mappingIndex].ContainsKey(initialValue)) return Map(initialValue, mappings, mappingIndex + 1); // cas de "on a pas de mapping" => la valeur reste la même
        return Map(mappings[mappingIndex][initialValue], mappings, mappingIndex + 1); // sinon on va chercher le mapping 
    }
}